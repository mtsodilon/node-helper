var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

app.get('/test-api', async (req, res) => {
    res.send('Essa api está funcionando OK.');
});

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);